package net.sssubtlety.discontinuous_beacon_beams;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

import java.util.List;

import static net.sssubtlety.discontinuous_beacon_beams.DiscontinuousBeaconBeams.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class Config implements ConfigData {
    @ConfigEntry.Gui.Tooltip
    public List<String> beam_hiding_blocks = FeatureControl.Defaults.beam_hiding_blocks;

    @ConfigEntry.Gui.Tooltip
    public boolean hiding_blocks_toggle_beams = FeatureControl.Defaults.hiding_blocks_toggle_beams;

    @ConfigEntry.Gui.Tooltip
    public boolean remember_color_when_hiding_blocks_enable_beams =
        FeatureControl.Defaults.remember_color_when_hiding_blocks_enable_beams;

    @ConfigEntry.Gui.Tooltip
    public boolean stainable_blocks_enable_beams = FeatureControl.Defaults.stainable_blocks_enable_beams;

    @ConfigEntry.Gui.Tooltip
    public boolean remember_color_when_stainable_blocks_enable_beams =
        FeatureControl.Defaults.remember_color_when_stainable_blocks_enable_beams;

    @ConfigEntry.Gui.Tooltip
    public boolean enable_beacon_beams = FeatureControl.Defaults.enable_beacon_beams;
}
