package net.sssubtlety.discontinuous_beacon_beams;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.text.Text;

public class DiscontinuousBeaconBeams {
    public static final String NAMESPACE = "discontinuous_beacon_beams";

    public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");

    public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);
}
