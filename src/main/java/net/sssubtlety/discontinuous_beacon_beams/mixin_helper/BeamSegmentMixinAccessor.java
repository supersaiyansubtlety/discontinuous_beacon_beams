package net.sssubtlety.discontinuous_beacon_beams.mixin_helper;

public interface BeamSegmentMixinAccessor {
    void discontinuous_beacon_beams$setInvisible();
    boolean discontinuous_beacon_beams$isVisible();
    void discontinuous_beacon_beams$setColor(int color);
}
