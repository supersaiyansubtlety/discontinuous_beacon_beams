package net.sssubtlety.discontinuous_beacon_beams.mixin_helper;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public enum SpoofBlock {
    STAINABLE(Blocks.BEACON),
    UN_STAINABLE(Blocks.BEDROCK),
    ORIGINAL(null);

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public final Optional<Block> block;

    SpoofBlock(@Nullable Block block) {
        this.block = Optional.ofNullable(block);
    }
}
