package net.sssubtlety.discontinuous_beacon_beams.mixin;

import net.minecraft.block.entity.BeaconBlockEntity;
import net.sssubtlety.discontinuous_beacon_beams.mixin_helper.BeamSegmentMixinAccessor;
import org.spongepowered.asm.mixin.*;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(BeaconBlockEntity.BeamSegment.class)
abstract class BeamSegmentMixin implements BeamSegmentMixinAccessor {
    @Shadow
    @Final
    @Mutable
    int color;

    @Unique
    private boolean visible;

    @Inject(method = "<init>", at = @At("TAIL"))
    private void initVisible(CallbackInfo ci) {
        this.visible = true;
    }

    @Override
    public void discontinuous_beacon_beams$setInvisible() {
        this.visible = false;
    }

    @Override
    public boolean discontinuous_beacon_beams$isVisible() {
        return this.visible;
    }

    @Override
    public void discontinuous_beacon_beams$setColor(int color) {
        this.color = color;
    }
}
