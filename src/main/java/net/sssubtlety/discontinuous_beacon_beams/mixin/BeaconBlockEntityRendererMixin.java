package net.sssubtlety.discontinuous_beacon_beams.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.v2.WrapWithCondition;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BeaconBlockEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.sssubtlety.discontinuous_beacon_beams.mixin_helper.BeamSegmentMixinAccessor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Slice;

import static net.sssubtlety.discontinuous_beacon_beams.FeatureControl.enableBeaconBeams;

@Mixin(BeaconBlockEntityRenderer.class)
abstract class BeaconBlockEntityRendererMixin {
    @Unique
    private static final String RENDER_METHOD =
        "render(Lnet/minecraft/block/entity/BeaconBlockEntity;FLnet/minecraft/client/util/math/MatrixStack;" +
        "Lnet/minecraft/client/render/VertexConsumerProvider;II)V";

    @ModifyExpressionValue(
        method = RENDER_METHOD,
        allow = 1,
        slice = @Slice(
            from = @At(
                value = "INVOKE",
                target = "Lnet/minecraft/block/entity/BeaconBlockEntity;getBeamSegments()Ljava/util/List;"),
            to = @At(
                value = "INVOKE",
                target = "Ljava/util/List;get(I)Ljava/lang/Object;")
        ),
        at = @At(value = "INVOKE", target = "Ljava/util/List;size()I")
    )
    private int skipLoopIfBeamsDisabled(int original) {
        return enableBeaconBeams() ? original : 0;
    }

    @ModifyExpressionValue(
        method = RENDER_METHOD,
        allow = 1,
        slice = @Slice(
            from = @At(
                value = "INVOKE",
                target = "Lnet/minecraft/block/entity/BeaconBlockEntity;getBeamSegments()Ljava/util/List;"
            ),
            to = @At(
                value = "INVOKE",
                target = "Lnet/minecraft/client/render/block/entity/BeaconBlockEntityRenderer;drawSegment(" +
                    "Lnet/minecraft/client/util/math/MatrixStack;" +
                    "Lnet/minecraft/client/render/VertexConsumerProvider;FJIII)V"
            )
        ),
        at = @At(value = "INVOKE", target = "Ljava/util/List;get(I)Ljava/lang/Object;")
    )
    private Object storeSegmentVisibility(
        Object beamSegment,
        @Share("currentSegmentIsVisible") LocalBooleanRef currentSegmentIsVisible
    ) {
        currentSegmentIsVisible.set(
            ((BeamSegmentMixinAccessor)beamSegment).discontinuous_beacon_beams$isVisible()
        );

        return beamSegment;
    }

    @WrapWithCondition(
        method = RENDER_METHOD,
        allow = 1, require = 1,
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/render/block/entity/BeaconBlockEntityRenderer;drawSegment(" +
                "Lnet/minecraft/client/util/math/MatrixStack;" +
                "Lnet/minecraft/client/render/VertexConsumerProvider;FJIII)V"
        )
    )
    private boolean renderIfVisible(
        MatrixStack matrices, VertexConsumerProvider vertexConsumers, float f, long x, int i, int j, int k,
        @Share("currentSegmentIsVisible") LocalBooleanRef currentSegmentIsVisible
    ) {
        return currentSegmentIsVisible.get();
    }
}
