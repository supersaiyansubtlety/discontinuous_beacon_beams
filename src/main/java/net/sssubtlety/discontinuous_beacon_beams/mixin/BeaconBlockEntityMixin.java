package net.sssubtlety.discontinuous_beacon_beams.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Stainable;
import net.minecraft.block.entity.BeaconBlockEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.sssubtlety.discontinuous_beacon_beams.mixin_helper.BeamSegmentMixinAccessor;
import net.sssubtlety.discontinuous_beacon_beams.mixin_helper.SpoofBlock;
import org.jetbrains.annotations.Nullable;
import org.objectweb.asm.Opcodes;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArgs;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.invoke.arg.Args;

import java.util.List;

import static net.sssubtlety.discontinuous_beacon_beams.FeatureControl.*;
import static net.sssubtlety.discontinuous_beacon_beams.mixin_helper.SpoofBlock.*;

@Mixin(BeaconBlockEntity.class)
abstract class BeaconBlockEntityMixin extends BlockEntity {
    @Unique
    private static final int DEFAULT_COLOR = ((Stainable) Blocks.BEACON).getColor().getOpaqueColor();

    // no two dye colors differ by 1, so DEFAULT_COLOR - 1 will never match
    @Unique
    private static final int UNMATCHABLE_DYE_COLOR = DEFAULT_COLOR - 1;

    // this must be a field so it persists across ticks
    @Nullable
    @Unique
    private Integer colorOverride;

    // this must be a field so it persists across ticks
    @Unique
    private boolean hasColor;

    // this must be a field so it persists across ticks
    @Unique
    private boolean beamIsVisible;

    @Shadow
    private final List<BeaconBlockEntity.BeamSegment> beamSegmentsToCheck;

    private BeaconBlockEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null, null);
        throw new IllegalStateException("BeaconBlockEntityMixin's dummy constructor called!");
    }

    @ModifyExpressionValue(method = "tick", at = @At(
        value = "INVOKE",
        target = "Lnet/minecraft/block/BlockState;getBlock()Lnet/minecraft/block/Block;"
    ))
    private static Block updateBeamStateAndSpoofBlock(
        final Block original, World world, BlockPos pos,
        BlockState unused, BeaconBlockEntity beaconBlockEntity,
        @Share("togglingVisibility") LocalBooleanRef togglingVisibility
    ) {
        if (
            !world.isClient ||
            original == Blocks.BEDROCK
        ) {
            return original;
        }

        return updateBeamStateAndSpoofBlockImpl(
            original, getTopSegment(beaconBlockEntity),
            ((BeaconBlockEntityMixin) (Object) beaconBlockEntity),
            togglingVisibility
        ).block.orElse(original);
    }

    @ModifyArgs(method = "tick", at = @At(value = "INVOKE", target = "Ljava/util/List;add(Ljava/lang/Object;)Z"))
    private static void modifyBeamState(
        Args args, World world, BlockPos pos, BlockState state,
        BeaconBlockEntity beaconBlockEntity
    ) {
        if (
            !enableBeaconBeams() ||
            getTopSegment(beaconBlockEntity) == null
        ) {
            return;
        }

        final BeamSegmentMixinAccessor beamSegment = args.get(0);

        final BeaconBlockEntityMixin beaconBlockEntityMixin = (BeaconBlockEntityMixin) (Object) beaconBlockEntity;
        //noinspection DataFlowIssue
        if (!beaconBlockEntityMixin.beamIsVisible) {
            beamSegment.discontinuous_beacon_beams$setInvisible();
        }

        if (beaconBlockEntityMixin.colorOverride != null) {
            beamSegment.discontinuous_beacon_beams$setColor(beaconBlockEntityMixin.colorOverride);
        }
    }

    // mismatch color to ensure a new segment is created when toggling visibility
    @ModifyExpressionValue(
        method = "tick",
        allow = 1,
        slice = @Slice(
            to = @At(
                value = "INVOKE",
                target = "Lnet/minecraft/block/entity/BeaconBlockEntity$BeamSegment;increaseHeight()V"
            )
        ),
        at = @At(
            value = "FIELD", opcode = Opcodes.GETFIELD,
            target = "Lnet/minecraft/block/entity/BeaconBlockEntity$BeamSegment;color:I"
        )
    )
    private static int mismatchColorIfTogglingVisibility(
        int original,
        @Share("togglingVisibility") LocalBooleanRef togglingVisibility
    ) {
        // the returned value is compared to a dye color
        return togglingVisibility.get() ? UNMATCHABLE_DYE_COLOR : original;
    }

    @Unique
    private static SpoofBlock updateBeamStateAndSpoofBlockImpl(
        final Block original, @Nullable BeaconBlockEntity.BeamSegment beamSegment,
        BeaconBlockEntityMixin beaconBlockEntityMixin,
        LocalBooleanRef togglingVisibility
    ) {
        if (beamSegment == null) {
            // inside source beacon block, initialize
            beaconBlockEntityMixin.hasColor = false;
            beaconBlockEntityMixin.beamIsVisible = true;

            return ORIGINAL;
        }

        if (!enableBeaconBeams()) {
            // prevent unnecessary stainable handling when not rendering
            return UN_STAINABLE;
        }

        // togglingVisibility shouldn't carry over between loop iterations: reset it
        togglingVisibility.set(false);

        final boolean beamWasVisible = beaconBlockEntityMixin.beamIsVisible;
        if (beamWasVisible) {
            if (doesHideBeam(original)) {
                beaconBlockEntityMixin.beamIsVisible = false;
                togglingVisibility.set(true);

                beaconBlockEntityMixin.colorOverride = beaconBlockEntityMixin.hasColor ?
                    beamSegment.getColor() : null;

                return STAINABLE;
            } else {
                if (original instanceof Stainable stainable) {
                    beaconBlockEntityMixin.colorOverride = beaconBlockEntityMixin.hasColor ?
                        null : stainable.getColor().getOpaqueColor();

                    beaconBlockEntityMixin.hasColor = true;
                }

                return ORIGINAL;
            }
        } else {
            if (hidingBlocksToggleBeams() && doesHideBeam(original)) {
                beaconBlockEntityMixin.beamIsVisible = true;
                togglingVisibility.set(true);

                if (rememberColorWhenHidingBlocksEnableBeams()) {
                    beaconBlockEntityMixin.colorOverride = beamSegment.getColor();
                    // hasColor goes unchanged
                } else {
                    beaconBlockEntityMixin.colorOverride = DEFAULT_COLOR;
                    beaconBlockEntityMixin.hasColor = false;
                }

                return STAINABLE;
            } else {
                if (stainableBlocksEnableBeams() && original instanceof Stainable stainable) {
                    beaconBlockEntityMixin.beamIsVisible = true;
                    togglingVisibility.set(true);

                    beaconBlockEntityMixin.colorOverride =
                        beaconBlockEntityMixin.hasColor && rememberColorWhenStainableBlocksEnableBeams() ?
                            null : stainable.getColor().getOpaqueColor();

                    beaconBlockEntityMixin.hasColor = true;
                }

                return ORIGINAL;
            }
        }
    }

    @Unique
    @Nullable
    private static BeaconBlockEntity.BeamSegment getTopSegment(BeaconBlockEntity beaconBlockEntity) {
        final BeaconBlockEntityMixin beaconBlockEntityMixin = (BeaconBlockEntityMixin) (Object) beaconBlockEntity;
        //noinspection DataFlowIssue
        final int size = beaconBlockEntityMixin.beamSegmentsToCheck.size();
        return size < 1 ? null : beaconBlockEntityMixin.beamSegmentsToCheck.get(size - 1);
    }
}
