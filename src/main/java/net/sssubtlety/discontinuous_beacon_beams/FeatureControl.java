package net.sssubtlety.discontinuous_beacon_beams;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.registry.Registries;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static net.sssubtlety.discontinuous_beacon_beams.DiscontinuousBeaconBeams.LOGGER;
import static net.sssubtlety.discontinuous_beacon_beams.Util.isModLoaded;

import static java.lang.System.lineSeparator;

public class FeatureControl {
    @Nullable
    private static final Config CONFIG_INSTANCE;

    private static boolean verifySave = true;

    private static ImmutableSet<Block> beamHidingBlocks =
        buildBlockSetFromStrings(Defaults.beam_hiding_blocks).getLeft();

    private static ActionResult onConfigSave(ConfigHolder<Config> holder, Config config) {
        if (verifySave && invalidateConfig("Error saving config")) {
            verifySave = false;
            holder.save();
            verifySave = true;
            return ActionResult.FAIL;
        }

        return ActionResult.CONSUME;
    }

    private static ActionResult onConfigLoad(ConfigHolder<Config> holder, Config config) {
        if (invalidateConfig("Error loading config")) {
            holder.save();
        }

        return ActionResult.CONSUME;
    }

    private static boolean invalidateConfig(String errPrefix) {
        final Pair<ImmutableSet<Block>, Optional<StringBuilder>> buildResult = buildBlockSetFromStrings(
            CONFIG_INSTANCE == null ? Defaults.beam_hiding_blocks : CONFIG_INSTANCE.beam_hiding_blocks
        );

        beamHidingBlocks = buildResult.getLeft();
        final Optional<StringBuilder> errBuilder = buildResult.getRight();

        if (errBuilder.isPresent()) {
            LOGGER.error("{}: {}", errPrefix, errBuilder.get());

            return true;
        } else {
            return false;
        }
    }

    private static Pair<@NotNull ImmutableSet<Block>, Optional<StringBuilder>> buildBlockSetFromStrings(
        List<String> strings
    ) {
        final var blockSetBuilder = ImmutableSet.<Block>builder();
        final List<String> errIds = new LinkedList<>();
        for (final var string : strings) {
            final var id = Identifier.parse(string);

            Registries.BLOCK.getOrEmpty(id).ifPresentOrElse(
                blockSetBuilder::add,
                () -> errIds.add(id.toString())
            );
        }

        final int errCount = errIds.size();
        StringBuilder errBuilder = null;
        if (errCount > 0) {
            errBuilder = new StringBuilder();
            if (errCount == 1) {
                errBuilder.append("No item found for id '").append(errIds.get(0)).append("'").append(lineSeparator());
            } else {
                errBuilder.append("No items found for ids:").append(lineSeparator());
                for (final String errId : errIds) {
                    errBuilder.append("- ").append(errId).append(lineSeparator());
                }
            }
        }

        return new Pair<>(blockSetBuilder.build(), Optional.ofNullable(errBuilder));
    }

    static {
        if (isModLoaded("cloth-config", ">=7.0.72")) {
            final ConfigHolder<Config> holder = AutoConfig.register(Config.class, GsonConfigSerializer::new);
            CONFIG_INSTANCE = holder.getConfig();
            onConfigLoad(holder, CONFIG_INSTANCE);
            holder.registerLoadListener(FeatureControl::onConfigLoad);
            holder.registerSaveListener(FeatureControl::onConfigSave);
        } else {
            CONFIG_INSTANCE = null;
        }
    }

    public interface Defaults {
        List<String> beam_hiding_blocks = Lists.newArrayList(
            Registries.BLOCK.getId(Blocks.GLASS).toString(),
            Registries.BLOCK.getId(Blocks.GLASS_PANE).toString()
        );
        boolean hiding_blocks_toggle_beams = true;
        boolean remember_color_when_hiding_blocks_enable_beams = true;
        boolean stainable_blocks_enable_beams = true;
        boolean remember_color_when_stainable_blocks_enable_beams = true;
        boolean enable_beacon_beams = true;
    }

    public static void init() { }

    public static boolean doesHideBeam(Block block) {
        return beamHidingBlocks.contains(block);
    }

    public static boolean hidingBlocksToggleBeams() {
        return CONFIG_INSTANCE == null ? Defaults.hiding_blocks_toggle_beams
            : CONFIG_INSTANCE.hiding_blocks_toggle_beams;
    }

    public static boolean rememberColorWhenHidingBlocksEnableBeams() {
        return CONFIG_INSTANCE == null ? Defaults.remember_color_when_hiding_blocks_enable_beams
            : CONFIG_INSTANCE.remember_color_when_hiding_blocks_enable_beams;
    }

    public static boolean stainableBlocksEnableBeams() {
        return CONFIG_INSTANCE == null ? Defaults.stainable_blocks_enable_beams
            : CONFIG_INSTANCE.stainable_blocks_enable_beams;
    }

    public static boolean rememberColorWhenStainableBlocksEnableBeams() {
        return CONFIG_INSTANCE == null ? Defaults.remember_color_when_stainable_blocks_enable_beams
            : CONFIG_INSTANCE.remember_color_when_stainable_blocks_enable_beams;
    }

    public static boolean enableBeaconBeams() {
        return CONFIG_INSTANCE == null ? Defaults.enable_beacon_beams : CONFIG_INSTANCE.enable_beacon_beams;
    }

    public static boolean isConfigLoaded() {
        return CONFIG_INSTANCE != null;
    }
}
