- 1.3.0 (17 Dec. 2024):
  - Marked as compatible with 1.21.4
  - Minor internal changes
  - Increased minimum Minecraft version to 1.21.2
- 1.2.3 (12 Nov. 2024): Marked as compatible with 1.21.2-1.21.3
- 1.2.2 (12 Aug. 2024):
  - Marked as compatible with 1.21.1
  - Fixed issues introduced in 1.2.1 with blocks 10 or more blocks above a beacon
  ([#7](https://gitlab.com/supersaiyansubtlety/discontinuous_beacon_beams/-/issues/7))
- 1.2.1 (6 Aug. 2024):
  - Fixed an issue where a beam wouldn't be hidden or unhidden as expected if the first block in its path was
  white stained glass or clear glass
  ([#6](<https://gitlab.com/supersaiyansubtlety/discontinuous_beacon_beams/-/issues/6>))
  - Theoretically improved the way beams are disabled when "Enable beacon beams" is false
    - performance should be (minutely) improved
    - compatibility should be slightly better
    (other mods' non-beam beacon rendering changes are more likely to remain enabled)
  - moderate internal changes
- 1.2.0 (11 Jul. 2024):
  - Updated for 1.21!
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
  [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates
  - Minor internal changes
- 1.1.15 (8 May 2024):
  - Marked as compatible with 1.20.6
  - Added discord link, viewable with [Mod Menu](https://modrinth.com/mod/modmenu)
- 1.1.14 (26 Apr. 2024):
  - Marked as compatible with 1.20.5
  - Minor internal changes
- 1.1.13 (26 Mar. 2024):
  - Suggest instead of recommend optional mods (no log spam)
  - Make Cloth Config actually optional (oops)
  - Add translation keys for Mod Menu fields and the screen that shows when 
Mod Menu but not Cloth Config is installed
- 1.1.12 (25 Mar. 2024):
  - Fixed compat with [BeamPass](https://modrinth.com/mod/beampass)
  - Minor internal changes
- 1.1.11 (30 Jan. 2024):
  - Marked as compatible with 1.20.3 and 1.20.4
  - Changed the default values of "Hiding blocks toggle beams" and "Stained blocks enable beams" to `true`
  - Minor internal changes
- 1.1.10 (27 Oct. 2023): Updated for 1.20.2
- 1.1.9 (21 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.1.8 (1 Apr. 2023): Updated for 1.19.3 and 1.19.4
- 1.1.7 (28 Nov. 2022): Fixed beam rendering when opaque blocks were in the way if those blocks were added to the
config.
- 1.1.6 (6 Aug. 2022): Marked as compatible with 1.19.2
- 1.1.5 (28 Jul. 2022): Fixed beam_hiding_blocks list not using user-configured values on game launch.
- 1.1.4 (28 Jul. 2022): Updated for 1.19.1
- 1.1.3 (23 Jun. 2022): Updated for 1.19!
- 1.1.2 (21 May 2022): 

  - Updated for 1.18.2!
  - cloth config is now optional
  
- 1.1.1 (13 Dec. 2021): Marked as compatible with 1.18.1
- 1.1 (7 Dec. 2021):

  Updated for 1.18!

  Configs no longer require a restart to take effect!

  Added new options!
    - Hiding blocks toggle beams (defaults to false): If an invisible beam passes through a block that hides beams,
    the beam becomes visible again.
    - Remember color when hiding blocks enable beams (defaults to true): When a block that hides beams makes a beam
    visible again, remember the beam color from before it became invisible.
    - Stained blocks enable beams (defaults to false): If an invisible beam passes through a block that changes beams'
    colors, the beam becomes visible again.
    - Remember color when dyed blocks enable beams (defaults to true): When a block that changes beams' colors makes a
    beam visible again, remember the beam color from before it became invisible.
    - Enable beacon beams (defaults to true): Enable or completely disable beacon beams.

- 1.0.4 (9 Jul. 2021): Marked as compatible with 1.17.1.
- 1.0.3-1 (15 Jun. 2021): Added missing dependency declarations so that fabric will warn you if you start the game
without required dependencies, rather than crashing later on. 
- 1.0.3 (15 Jun. 2021): Updated for 1.17.
- 1.0.2 (16 Jan. 2021): Marked as compatible with 1.16.5.
- 1.0.1 (11 Jan. 2021): Fixed translation downloads.
- 1.0 (27 Dec. 2020): Initial release.
- 0.0.1 (25 Dec. 2020): Initial version.
